CREATE TABLE "user"
(
    uuid_user uuid DEFAULT uuid_generate_v4 (),
    
    user_name varchar(255),
    user_login varchar(255) NOT NULL UNIQUE,
    user_password varchar(255),
    post_imageName varchar(255),
    user_location varchar(255),
    user_birthDate DATE,
    user_email varchar(255) NOT NULL UNIQUE,
    user_description text,
    primary key (uuid_user)
);