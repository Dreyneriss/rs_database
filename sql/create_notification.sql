CREATE TABLE "notification"
(
    uuid_notification uuid DEFAULT uuid_generate_v4 (),
    uuid_commentaire uuid NOT NULL,
    uuid_user uuid NOT NULL,
    
    notification_comment TEXT,
    notification_date TIMESTAMP NOT NULL,
    primary key (uuid_notification)
);
