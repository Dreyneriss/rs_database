CREATE TABLE "post"
(
    uuid_post uuid DEFAULT uuid_generate_v4 (),
    uuid_user uuid NOT NULL,

    post_content TEXT,
    post_imageName  varchar(255),
    post_date TIMESTAMP NOT NULL,
    post_edit_date TIMESTAMP,
    post_nb_like INT,
    primary key (uuid_post)
);
