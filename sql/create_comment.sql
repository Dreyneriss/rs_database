CREATE TABLE "commentaire"
(
    uuid_comment uuid DEFAULT uuid_generate_v4 (),
    uuid_post uuid NOT NULL,
    uuid_user uuid NOT NULL,

    comment_date TIMESTAMP NOT NULL,
    comment_edit_date TIMESTAMP,
    comment TEXT NOT NULL,
    primary key (uuid_comment)
);
