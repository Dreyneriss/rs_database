CREATE TABLE "like"
(
    uuid_post uuid NOT NULL,
    uuid_user uuid NOT NULL,
    
    primary key (uuid_post, uuid_user)
);

CREATE TABLE "notification"
(
    uuid_notification uuid DEFAULT uuid_generate_v4 (),
    uuid_commentaire uuid NOT NULL,
    uuid_user uuid NOT NULL,
    
    notification_comment TEXT,
    notification_date TIMESTAMP NOT NULL,
    primary key (uuid_notification)
);

CREATE TABLE "partage"
(
    uuid_share uuid DEFAULT uuid_generate_v4 (),
    uuid_post uuid NOT NULL,
    uuid_share_user uuid NOT NULL,
    uuid_author_user uuid NOT NULL,

    primary key (uuid_share)
);

CREATE TABLE "commentaire"
(
    uuid_comment uuid DEFAULT uuid_generate_v4 (),
    uuid_post uuid NOT NULL,
    uuid_user uuid NOT NULL,

    comment_date TIMESTAMP NOT NULL,
    comment_edit_date TIMESTAMP,
    comment TEXT NOT NULL,
    primary key (uuid_comment)
);

CREATE TABLE "follow"
(
    uuid_follower uuid uuid NOT NULL,
    uuid_user_followed uuid NOT NULL,
    primary key (uuid_follower, uuid_user_followed)
);
