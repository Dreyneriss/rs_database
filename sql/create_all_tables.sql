CREATE TABLE "user"
(
    uuid_user uuid DEFAULT uuid_generate_v4 (),
    
    user_name varchar(255),
    user_login varchar(255) NOT NULL UNIQUE,
    user_password varchar(255),
    post_imageName varchar(255),
    user_location varchar(255),
    user_birthDate DATE,
    user_email varchar(255) NOT NULL UNIQUE,
    user_description text,
    primary key (uuid_user)
);

CREATE TABLE "post"
(
    uuid_post uuid DEFAULT uuid_generate_v4 (),
    uuid_user uuid NOT NULL,

    post_content TEXT,
    post_imageName  varchar(255),
    post_date TIMESTAMP NOT NULL,
    post_edit_date TIMESTAMP,
    post_nb_like INT,
    primary key (uuid_post)
);

CREATE TABLE "like"
(
    uuid_post uuid NOT NULL,
    uuid_user uuid NOT NULL,
    
    primary key (uuid_post, uuid_user)
);

CREATE TABLE "notification"
(
    uuid_notification uuid DEFAULT uuid_generate_v4 (),
    uuid_commentaire uuid NOT NULL,
    uuid_user uuid NOT NULL,
    
    notification_comment TEXT,
    notification_date TIMESTAMP NOT NULL,
    primary key (uuid_notification)
);

CREATE TABLE "partage"
(
    uuid_share uuid DEFAULT uuid_generate_v4 (),
    uuid_post uuid,
    uuid_share_user uuid NOT NULL,
    uuid_author_user uuid,
    share_date TIMESTAMP NOT NULL,

    primary key (uuid_share)
);

CREATE TABLE "commentaire"
(
    uuid_comment uuid DEFAULT uuid_generate_v4 (),
    uuid_post uuid NOT NULL,
    uuid_user uuid NOT NULL,

    comment_date TIMESTAMP NOT NULL,
    comment_edit_date TIMESTAMP,
    comment TEXT NOT NULL,
    primary key (uuid_comment)
);
