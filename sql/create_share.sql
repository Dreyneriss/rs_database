CREATE TABLE "partage"
(
    uuid_share uuid DEFAULT uuid_generate_v4 (),
    uuid_post uuid,
    uuid_share_user uuid NOT NULL,
    uuid_author_user uuid,
    share_date TIMESTAMP NOT NULL,

    primary key (uuid_share)
);