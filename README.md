Ce projet permet de créer les tables et de les remplir pour notre base de données avec des données random en grande quantité. Mais sans les images.

Normalement, il suffit de lancer l'un ou l'autre des deux scripts bash.
Dans les deux cas, ça nécessite l'entrée d'un même mot de passe... plein de fois !

```create_and_fill_local.sh```\
Celui-là est fait pour une unique base de données en local.\
Elle nécessite que vous remplaciez tous les ```user``` par votre nom d'utilisateur postgres, et tous les ```database``` par le nom de votre base de données.\
Vous devrez entrer 9 fois votre mot de passe. Pensez à le copier.

```create_and_fill_scaleway.sh```\
Je ne l'ai pas encore essayé.\
Étant donné qu'on a une base de donnée par api, j'ai dû séparer les créations de tables et les connections pour les créer et les remplir, ce qui augmente le nombre de mdp à entrer...

#### Source utilisée pour les prénoms, noms de famille et proverbes servant de commentaires :
https://fr.wikipedia.org/wiki/Liste_des_noms_de_famille_les_plus_courants_en_France \
https://fr.wikipedia.org/wiki/Liste_de_pr%C3%A9noms_en_fran%C3%A7ais

https://fr.wiktionary.org/wiki/Annexe:Liste_de_proverbes_fran%C3%A7ais
