echo "Cleaning existing tables"
psql -d reseau_social -U back_admin -f sql/drop_all_tables.sql

echo "Creating and filling user table"
psql -d reseau_social -U back_admin -f sql/create_user.sql
python python/gen_fill_user.py
psql -d reseau_social -U back_admin -f sql/fill_user.sql

echo "Generating events"
python ./python/gen_events.py
cut -f1 datas/list_of_events.tsv > temp1
cut -f2 datas/list_of_events.tsv | sort > temp2
cut -f3- datas/list_of_events.tsv > temp3
paste temp1 temp2 temp3 > datas/list_of_events.tsv 
rm temp1 temp2 temp3
grep "post" datas/list_of_events.tsv > datas/list_of_posts.tsv
grep "like" datas/list_of_events.tsv > datas/list_of_likes.tsv
grep "share" datas/list_of_events.tsv > datas/list_of_shares.tsv
grep "comment" datas/list_of_events.tsv > datas/list_of_comments.tsv

echo "creating the list of users"
psql -d reseau_social -U back_admin -c 'select * from "user";' | tail -n+3 | head -n-2 | cut -d' ' -f2 > datas/list_of_users
echo "creating the post table and queries"
psql -d reseau_social -U back_admin -f sql/create_post.sql
python python/gen_fill_post.py
echo "filling the post table"
psql -d reseau_social -U back_admin -f sql/fill_post.sql

echo "creating the list of posts"
psql -d reseau_social -U back_admin -c 'select * from "post";' | tail -n+3 | head -n-2 | cut -d' ' -f2 > datas/list_of_posts
echo "generating the remaining queries"
python python/gen_fill_rest.py
psql -d reseau_social -U back_admin -f sql/create_rest_of_tables.sql
echo "Filling the remaining tables"
psql -d reseau_social -U back_admin -f sql/fill_from_user_and_post.sql
