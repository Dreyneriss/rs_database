import random
import calendar
import numpy as np

def twoDigit(x):
    if(x < 10):
        return '0'+str(x)
    else:
        return str(x)

fileObj = open('datas/list_of_users', "r") #opens the file in read mode
users = fileObj.read().splitlines() #puts the file into an array
fileObj.close()

fileObj = open('datas/list_of_proverbes', "r") #opens the file in read mode
proverbes = fileObj.read().splitlines() #puts the file into an array
fileObj.close()

fileObj = open('datas/list_of_events.tsv', "w")

nbEvents = 200

#fileObj.write("type\tdate\tid_post\tid_user\tmisc\n")
#first event has to be a post
posts = np.array([0])
age = random.randrange(0,3) # 2 years old app, reasonable
year = 2021 - age
month = random.randrange(1,13)
day = twoDigit(random.randrange(1,calendar.monthrange(year,month)[1]+1))
month = twoDigit(month)
hour = twoDigit(random.randrange(0,24))
minute = twoDigit(random.randrange(0,60))
second = twoDigit(random.randrange(0,60))
date = str(year)+"-"+month+"-"+day+" "+hour+":"+minute+":"+second
user = random.choice(users)
authors = np.array([user]) #used for the share event
proverbe = random.choice(proverbes)
fileObj.write("post\t"+date+"\t0\t"+user+"\t"+proverbe+"\n")

for i in range(nbEvents):
    user = random.choice(users)
    
    #dates will sorted afterward
    age = random.randrange(0,3) # 2 years old app, reasonable
    year = 2021 - age
    month = random.randrange(1,13)
    day = twoDigit(random.randrange(1,calendar.monthrange(year,month)[1]+1))
    month = twoDigit(month)
    hour = twoDigit(random.randrange(0,24))
    minute = twoDigit(random.randrange(0,60))
    second = twoDigit(random.randrange(0,60))
    date = str(year)+"-"+month+"-"+day+" "+hour+":"+minute+":"+second

    event_type = random.randrange(0,100)
    if(event_type < 15):   # USER SEND POST
        post = posts.size
        posts = np.append(posts,post)
        proverbe = random.choice(proverbes)
        authors = np.append(authors,user)
        fileObj.write("post\t"+date+"\t"+str(post)+"\t"+user+"\t"+proverbe+"\n")
    elif(event_type < 50): # USER LIKE POST
        post = random.choice(posts) #could add a law so that elders are less picked
        fileObj.write("like\t"+date+"\t"+str(post)+"\t"+user+"\t\n")
    elif(event_type < 75): # USER SHARE POST
        p = random.randrange(posts.size)
        post = posts[p]
        user2 = authors[p]
        fileObj.write("share\t"+date+"\t"+str(post)+"\t"+user+"\t"+user2+"\n")
    elif(event_type < 95): # USER COMMENT POST
        post = random.choice(posts)
        comment = random.choice(proverbes)
        fileObj.write("comment\t"+date+"\t"+str(post)+"\t"+user+"\t"+comment+"\n")
    else:                 # USER FOLLOW USER 2
        user = random.choice(users)
        user2 = random.choice(users)
        fileObj.write("follow\t"+date+"\t\t"+user+"\t"+user2+"\n")

fileObj.close()

