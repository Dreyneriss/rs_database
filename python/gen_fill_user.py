import random
import calendar

fileObj = open('datas/list_of_firstnames', "r") #opens the file in read mode
firstnames = fileObj.read().splitlines() #puts the file into an array
fileObj.close()

fileObj = open('datas/list_of_surnames', "r") #opens the file in read mode
surnames = fileObj.read().splitlines() #puts the file into an array
fileObj.close()

fileObj = open('sql/fill_user.sql', "w") #opens the file in read mode

nbFill = 10

for i in range(nbFill):
    first = random.choice(firstnames)
    sur = random.choice(surnames)
    name = first + " " + sur

    # varying : only first letter of first or surname ; pref of first + suf of sur ; random fiction name...
    login = first + "." + sur

    # how will it work ?
    pic = ""

    # list of cities, including fiction because lol
    location = ""

    age = random.randrange(7,78) # Barnum effect
    year = 2021 - age
    month = random.randrange(1,13)
    day = random.randrange(1,calendar.monthrange(year,month)[1]+1)
    birthDate = str(year)+"-"+str(month)+"-"+str(day)
    email = login + "@nomail.pwet"

    fileObj.write("INSERT INTO \"user\"(user_name, user_login, user_imageURL, user_location, user_birthDate, user_email) VALUES ('"+name+"', '"+login+"', '"+pic+"', '"+location+"', '"+birthDate+"', '"+email+"');\n")

fileObj.close()

