import random
import calendar
import numpy
import pandas as pd

######################
# LOADING NEEDED DATA

comments = pd.read_csv('datas/list_of_comments-posts-users', delimiter=' ', header=None)

nbFillComment = comments[0].size

########################
# CREATING NOTIF QUERIES

fileObj = open('sql/fill_notification.sql', "w") #opens the file in read mode

for i in range(nbFillComment):
    comment = comments[0][i]
    user = comments[2][i]
    post = comments[1][i]
    date = comments[3][i]

    notification = "Un commentaire a été posté sur votre post."

    fileObj.write("INSERT INTO \"notification\"(uuid_commentaire, uuid_user, notification_comment, notification_date) VALUES ('"+comment+"', '"+user+"', '"+notification+"', '"+date+"');\n")

fileObj.close()
