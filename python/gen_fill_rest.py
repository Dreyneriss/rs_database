import random
import calendar
import numpy
import pandas as pd

fileObj = open('datas/list_of_users', "r") #opens the file in read mode
users = numpy.array(fileObj.read().splitlines()) #puts the file into an array
fileObj.close()

fileObj = open('datas/list_of_posts', "r") #opens the file in read mode
posts = numpy.array(fileObj.read().splitlines()) #puts the file into an array
fileObj.close()

shares = pd.read_csv('datas/list_of_shares.tsv', delimiter='\t', header=None)
comments = pd.read_csv('datas/list_of_comments.tsv', delimiter='\t', header=None)
likes = pd.read_csv('datas/list_of_likes.tsv', delimiter='\t', header=None)

fileObj = open('sql/fill_from_user_and_post.sql', "w") #opens the file in read mode

nbFillLikes = likes[0].size
nbFillShare = shares[0].size
nbFillComment = comments[0].size

for i in range(nbFillLikes):
    user = likes[3][i]
    post = posts[likes[2][i]]

    fileObj.write("INSERT INTO \"like\"(uuid_post, uuid_user) VALUES ('"+post+"', '"+user+"');\n")

for i in range(nbFillShare):
    user = shares[3][i]
    post = posts[shares[2][i]]
    user2 = shares[4][i]

    fileObj.write("INSERT INTO \"partage\"(uuid_post, uuid_share_user, uuid_author_user) VALUES ('"+post+"', '"+user+"', '"+user2+"');\n")


for i in range(nbFillComment):
    user = comments[3][i]
    post = posts[comments[2][i]]
    date = comments[1][i]
    comment = comments[4][i]

#    notification = "Un commentaire a été posté sur votre post."

    fileObj.write("INSERT INTO \"commentaire\"(uuid_post, uuid_user, comment_date, comment) VALUES ('"+post+"', '"+user+"', '"+date+"', '"+comment+"');\n")
#    fileObj.write("INSERT INTO \"notification\"(uuid_comment, uuid_user, notification_comment, notification_date) VALUES ('"+post+"', '"+user+"', '"+notification+"', '"+date+"');\n")

fileObj.close()
