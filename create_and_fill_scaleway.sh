psql -h 51.159.24.37 --port 36144  -d db_picarrot_users -U picarrotAdmin -c 'DELETE FROM "user"'
psql -h 51.159.24.37 --port 36144  -d db_picarrot_post -U picarrotAdmin -c 'DELETE FROM "post"'
psql -h 51.159.24.37 --port 36144  -d db_picarrot_like -U picarrotAdmin -c 'DELETE FROM "like"'
psql -h 51.159.24.37 --port 36144  -d db_picarrot_notification -U picarrotAdmin -c 'DELETE FROM "notification"'
psql -h 51.159.24.37 --port 36144  -d db_picarrot_partage -U picarrotAdmin -c 'DELETE FROM "partage"'
psql -h 51.159.24.37 --port 36144  -d db_picarrot_commentaire -U picarrotAdmin -c 'DELETE FROM "commentaire"'
psql -h 51.159.24.37 --port 36144  -d db_picarrot_follow -U picarrotAdmin -c 'DELETE FROM "follow"'

python python/gen_fill_user.py
psql -h 51.159.24.37 --port 36144  -d db_picarrot_users -U picarrotAdmin -f sql/fill_user.sql
psql -h 51.159.24.37 --port 36144  -d db_picarrot_users -U picarrotAdmin -c 'select * from "user";' | tail -n+3 | head -n-2 | cut -d' ' -f2 > datas/list_of_users

python ./python/gen_events.py
cut -f1 datas/list_of_events.tsv > temp1
cut -f2 datas/list_of_events.tsv | sort > temp2
cut -f3- datas/list_of_events.tsv > temp3
paste temp1 temp2 temp3 > datas/list_of_events.tsv
rm temp1 temp2 temp3
grep "post" datas/list_of_events.tsv > datas/list_of_posts.tsv
grep "like" datas/list_of_events.tsv > datas/list_of_likes.tsv
grep "share" datas/list_of_events.tsv > datas/list_of_shares.tsv
grep "comment" datas/list_of_events.tsv > datas/list_of_comments.tsv
grep "follow" datas/list_of_events.tsv > datas/list_of_follows.tsv

python python/gen_fill_post.py
psql -h 51.159.24.37 --port 36144  -d db_picarrot_post -U picarrotAdmin -f sql/fill_post.sql

psql -h 51.159.24.37 --port 36144  -d db_picarrot_post -U picarrotAdmin -c 'select * from "post";' | tail -n+3 | head -n-2 | cut -d' ' -f2 > datas/list_of_posts

python python/gen_fill_rest-scw.py
psql -h 51.159.24.37 --port 36144  -d db_picarrot_like -U picarrotAdmin -f sql/fill_like.sql
psql -h 51.159.24.37 --port 36144  -d db_picarrot_partage -U picarrotAdmin -f sql/fill_share.sql
psql -h 51.159.24.37 --port 36144  -d db_picarrot_commentaire -U picarrotAdmin -f sql/fill_comment.sql
psql -h 51.159.24.37 --port 36144  -d db_picarrot_follow -U picarrotAdmin -f sql/fill_follow.sql

psql -h 51.159.24.37 --port 36144  -d db_picarrot_commentaire -U picarrotAdmin -c 'select * from "commentaire";' | tail -n+3 | head -n-2 | cut -d' ' -f2,4,6,8 > datas/list_of_comments-posts-users
python python/gen_fill_notif.py

psql -h 51.159.24.37 --port 36144  -d db_picarrot_notification -U picarrotAdmin -f sql/fill_notification.sql
